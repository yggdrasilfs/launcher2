﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Launcher_New
{
    public partial class Main : Form
    {
        string verinfo = "";

        public Main()
        {
            InitializeComponent();
            Hide();
            if (File.Exists("lock"))
            {
                MessageBox.Show("Yggdrasil is already running!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
            try
            {
                string[] lines = new WebClient().DownloadString("https://updates.koyu.space/yggdrasil/launcher.txt").Split('\n');
                foreach (string s in lines)
                {
                    textBox1.Text += s + Environment.NewLine;
                }
            } catch {
                textBox1.Text = "Cannot get launcher info. Are you connected to the internet? It wouldn't make any sense if you're not, because at Yggdrasil it's all about servers.";
            }
            label1.Text = "Yggdrasil Launcher version: 2.5\nYggdrasil version: ";
            label2.Text = "© Yggdrasil Team " + Convert.ToString(DateTime.Now.Year);
            try {
                verinfo = File.ReadAllLines("verinfo")[0];
                label1.Text += verinfo;
            } catch {
                label1.Text += "not downloaded";
            }
            try
            {
                string a = "http://updates.koyu.space/yggdrasil/latest.txt";
                if (File.ReadAllText("updateserver") == "lts")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_lts.txt";
                }
                if (File.ReadAllText("updateserver") == "dev")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_dev.txt";
                }
                if (File.ReadAllText("updateserver") == "beta")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_beta.txt";
                }
                string latest = new WebClient().DownloadString(a).Split('\n')[0];
                if (verinfo != latest)
                {
                    if (File.Exists("Updater.exe"))
                    {
                        button2.Visible = true;
                    }
                    else
                    {
                        try
                        {
                            new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Launcher.exe", "Updater.exe");
                            button2.Visible = true;
                        }
                        catch { }
                    }
                }
            }
            catch { }
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 15, 15));
            Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!File.Exists("Yggdrasil.exe"))
            {
                try
                {
                    new WebClient().DownloadFile("https://updates.koyu.space/yggdrasil/Launcher.exe", "Updater.exe");
                }
                catch { }
                Process.Start("Updater.exe");
                Environment.Exit(0);
            }
            else
            {
                Process.Start("Yggdrasil.exe", ">> log.txt");
                Environment.Exit(0);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("Updater.exe");
                Environment.Exit(0);
            }
            catch { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private bool _dragging = false;
        private Point _start_point = new Point(0, 0);


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
         );

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Top -= 10;
        }
    }
}
